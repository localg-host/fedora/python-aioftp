%global srcname aioftp

Name:          python-%{srcname}
Summary:       FTP client/server for asyncio
License:       ASL 2.0
URL:           https://github.com/aio-libs/aioftp

Version:       0.13.0
Release:       1%{?dist}
Source0:       %{pypi_source}

BuildArch:     noarch


%global _description %{expand:
FTP client/server for asyncio.}

%description %_description


%package -n python3-%{srcname}
Summary:       %{summary}

BuildRequires: python3-devel
BuildRequires: %{py3_dist async-timeout}
BuildRequires: %{py3_dist pytest}
BuildRequires: %{py3_dist pytest-asyncio}
BuildRequires: %{py3_dist setuptools}
BuildRequires: %{py3_dist trustme}

%{?python_provide:%python_provide python3-%{srcname}}


%description -n python3-%{srcname} %_description


%prep
%autosetup -n %{srcname}-%{version} -p1

# This should never have been in the tarball in the first place, and tests fail
# on EL8 because of it
find . -name __pycache__ -prune -exec rm -rf {} +


%build
%py3_build


%install
%py3_install


%check
# Tests can't find the module without this
touch tests/__init__.py

py.test-3


%files -n python3-%{srcname}
%doc README.rst
%license license.txt
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py*.egg-info


%changelog
* Fri Nov 01 2019 Mathieu Bridon <bochecha@daitauha.fr> - 0.13.0-1
- Initial package for Fedora.
